#ifndef TZRESOLVER_HEADERS

#define TZRESOLVER_HEADERS

#include <iostream>
#include <vector>
#include <map>
#include "unicode/timezone.h"
#include "unicode/smpdtfmt.h"
#include "boost/date_time/local_time/local_time.hpp"

#define DEFAULT_LANG "eng"

namespace tzutils {

class facility;
class resolver;

typedef std::unique_ptr<icu::TimeZone> itz_ptr;
typedef std::shared_ptr<icu::Locale> locale_ptr;
typedef std::shared_ptr<icu::SimpleDateFormat> sdf_ptr;
typedef std::shared_ptr<facility> tzfy_ptr;
typedef std::unique_ptr<resolver> rslvr_ptr;

typedef std::vector<std::string> strings;
typedef std::vector<itz_ptr> icu_tzs;
typedef std::vector<tzfy_ptr> tzfys;

typedef std::map<std::string, locale_ptr> locales_map;
typedef std::map<std::string, sdf_ptr> formats_map;

/**
 * Returns current system datetime as UDate instance
 *
 */
UDate now();

/**
 * Resolver provides mapping between old-style tz abbreviations
 * and suspected ICU timezone objects.
 * It is an abstract class, therefore a particular approach
 * to tz names resolvation must be implemented in the subclasses.
 */
class resolver {
    friend class facility;
public:

    /**
     * For a given ambiguous timezone name
     * returns a list of timezone facilities
     * binded to timezones that the user might had in mind.
     *
     * @param ambiguous_name a timezone name as an abbreviation in upper case
     * @return set of founded facilities
     *
     */
    virtual tzfys list ( std::string ambiguous_name ) = 0;

    /**
     * Returns a facility corresponded to best matched timezone,
     * or throws an exception if nothing was found.
     * @param name a timezone name as an abbreviation in upper case
     * @throws -1 if no timezone was found
     * @return tz facility binded to the timezone and the locale
     * in which timezone format can be (possibly) represented
     * as an old-style tz abbreviation
     */
    virtual facility& best_match ( const std::string& name ) = 0;

    /**
     * Returns a facility corresponded to given tz name 
     * as it recognized by ICU library
     * or throws an exception if such canonical name was not found.
     * @param name a timezone name
     * as an abbreviation or as an IANA (Olson) name
     * @throws -1 if no timezone was found
     * @return tz facility binded to the timezone and the locale
     * in which timezone format can be (possibly) represented
     * as an old-style tz abbreviation
     */
    facility& iana ( const std::string& name ) const;

    /**
     * A factory method for the resolver
     */
    static resolver* create_resolver ( const std::string& language = DEFAULT_LANG );

protected:
    locales_map locales;
    /**
    * For a given timezone t returns a list of ISO 3166 codes of countries
    * which locale supports abbreviated TZ output
    */
    virtual strings abbr_supported_countries ( icu::TimeZone& t ) const = 0;
    /**
     * For given tz name returns canonical ICU TimeZone object
     */
    static icu::TimeZone& get_canonical_icu_tz ( const UnicodeString& name );
    static icu::TimeZone& get_canonical_icu_tz ( const std::string& name );
private:

    virtual std::string friendly_country ( const icu::TimeZone& t ) const = 0;
};

/**
 * A concrete class based on the resolver. Provides an implementation of
 * resolver interface by using lookup from boost timezone data source.
 */
class resolver_boost : public resolver {
    friend class facility;
public:
    resolver_boost ( const std::string& language );

    virtual tzfys list ( std::string ambiguous_name );
    virtual facility& best_match ( const std::string& name );
    virtual strings abbr_supported_countries ( icu::TimeZone& t ) const;

private:
    /**
     * For given timezone returns alphabetically first ISO 3166 country code
     * if it supports abbreviated timezone name, otherwise returns "GB"
     */
    std::string friendly_country ( const icu::TimeZone& t ) const;
    typedef boost::local_time::time_zone_ptr btz_ptr;
    typedef std::vector<std::pair<std::string, btz_ptr>> btzs;
    typedef std::multimap<std::string, itz_ptr> db_map;
    std::multimap<std::string, std::pair<tzfy_ptr, bool>> facilities_map;
    std::map<std::string, std::vector<std::string>> abbrs;
    boost::local_time::tz_database db;
    db_map abbr_map;
    strings names_list;
    btzs entries;
    formats_map tz_formatters;
    std::string lang;
    void load_entries() ;
    /**
     * Return boost tzdb entry for the given name
     *
     */
    btz_ptr get_tz_entry_by_id ( std::string name );

    void update_names_list();
    /**
     * Creates boost tzdb based on CSV file source
     */
    void fill_db();
    void build_locales ();
    void create_tz_formatters ( const std::string& fmt, UErrorCode& status );
    void create_facility();
    void add_facility ( std::string key, facility& tzfy, const bool& dst = false );
};


/**
 * Encapsulates a pair of given ICU Timezone and ICU Locale
 * in order to provide simple datetime formatting interface
 *
 */
class facility {
    friend class resolver_boost;
public:
    /**
     * For given timezone constructs new facility
     * in which binded locale will be automatically selected
     * using given resolver object in order to find best match for
     * pretty abbreviated old-style tz name formatting
     */
    facility ( const icu::TimeZone& t, const resolver& r )
        ;
    /**
     * Constructs new facility based on given timezone and locale
     */
    facility ( icu::TimeZone& t, icu::Locale& l );
    facility ( icu::Calendar& cal );
    /**
     * Returns ICU TimeZone object binded to this facility
     */
    const icu::TimeZone& tz() const;
    /**
     * Returns ICU Calendar object binded to this facility
     */
    const icu::Calendar& calendar() const;
    /**
     * Returns a datetime represented as string in a given format
     * @param dt UTC datetime represented as UDate object
     * @param fmt Format pattern, as described for ICU SimpleDateFormat
     */
    std::string format_datetime ( const UDate& dt, const std::string& fmt ) const;

private:
    typedef boost::shared_ptr<icu::TimeZone> tzptr;
    std::unique_ptr<icu::Locale> loc;
    std::unique_ptr<const icu::TimeZone> itz;
    std::unique_ptr<icu::Calendar> cal;
    UErrorCode status {U_ZERO_ERROR};
    strings countries;
    sdf_ptr fm;
    /**
     * Returns true if this facility is capable for pretty tz name
     * formatting, otherwise returns false.
     */
    const bool pretty() const;
    /**
     * Returns true if binded timezone is the same as
     * ICU IANA recognized timezone for given name
     */
    const bool match_iana_name ( const std::string& name ) const;
    /**
     * Returns true if current time in DST mode, otherwise false
     */
    const bool dst() const;
    const UDate now() const;
    std::string best_tzname_output();
    void setup(const icu::TimeZone& t, icu::Locale& l);
};


}

#endif
