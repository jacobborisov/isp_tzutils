#include <vector>
#include <iostream>
#include <iterator>
#include <memory>
#include <experimental/array>
#include <unistd.h>
#include <stdlib.h>
#include "boost/format.hpp"
#include "boost/algorithm/string/join.hpp"
#include "boost/date_time/local_time/local_time.hpp"
#include "boost/date_time/local_time/tz_database.hpp"
#include "boost/log/trivial.hpp"
#include <boost/range/join.hpp>
#include "resolver.hpp"


extern const char _binary_date_time_zonespec_csv_start, _binary_date_time_zonespec_csv_end;

namespace tzutils {


const std::string format_str = "E MMM d HH:mm:ss zzz y";


UErrorCode status ( U_ZERO_ERROR );
const icu::Calendar* calendar ( icu::Calendar::createInstance ( status ) );


UDate now() {
    return calendar->getNow();
}

static const strings iso_countries() {
    strings ss;
    auto ar = icu::Locale::getISOCountries();

    //using std::experimental::to_array;

    //auto s = std::experimental::to_array(*ar);

    // NOTE: maybe there are more idiomatic way to deal with this?
    int i = 0;

    while ( ar[i] ) {
        ss.push_back ( ar[i++] );
        //std::cout << ar[i++] << std::endl;
    }

    return ss;
}

sdf_ptr get_formatter ( const std::string& fmt, const icu::Locale& ll, UErrorCode& status ) {
    UnicodeString ufmt = fmt.c_str();
    return sdf_ptr ( new icu::SimpleDateFormat ( ufmt, ll, status ) );
}

resolver* resolver::create_resolver ( const std::string& language ) {
    return new resolver_boost ( language );
}

facility& resolver::iana ( const std::string& name ) const {
    icu::TimeZone* t = &get_canonical_icu_tz ( name );

    if ( *t == icu::TimeZone::getUnknown() ) {
        throw -1;
    }
    return * ( new facility ( *t, *this ) );
}
icu::TimeZone& resolver::get_canonical_icu_tz ( const std::string& name ) {
    UnicodeString orig_name = name.c_str();
    return get_canonical_icu_tz ( orig_name );
}

icu::TimeZone& resolver::get_canonical_icu_tz ( const UnicodeString& name ) {
    UErrorCode status = U_ZERO_ERROR;
    UnicodeString canonicalized_name;
    std::string temp, temp2;
    icu::TimeZone::getCanonicalID ( name, canonicalized_name, status );
    return *icu::TimeZone::createTimeZone ( canonicalized_name );
}

resolver_boost::resolver_boost ( const std::string& language ) {
    lang = language;
    build_locales();
    create_tz_formatters ( "zzz", status );
    fill_db();
    update_names_list();
    load_entries();
}

void resolver_boost::create_facility() {

}

void resolver_boost::add_facility ( std::string key, facility& tzfy, const bool& dst) {
    typedef std::multimap<std::string,std::pair<tzfy_ptr,bool>>::iterator iter;
    std::pair <iter, iter> ret;

    UnicodeString tmp;
    auto eq_id_name = icu::TimeZone::getEquivalentID ( tzfy.tz().getID ( tmp ), 0 );

    if ( eq_id_name.length() > 0 && eq_id_name != tmp ) {
        // create new facility?
        tzfy = facility ( get_canonical_icu_tz ( eq_id_name ), *this );
    }


    ret = facilities_map.equal_range ( key );
    for ( auto it = ret.first; it != ret.second; ++it ) {

        // skip if already presented in multimap
        if ( it->second.first->tz() == tzfy.tz() ) {
            return;
        }
    }


    boost::algorithm::to_upper ( key );

    facilities_map.insert ( std::make_pair ( key,std::make_pair( &tzfy, dst ) ));
}


/**
 * Saves full list of tz objects in the private field
 *
 */
void resolver_boost::load_entries() {
    BOOST_LOG_TRIVIAL ( debug ) << "Load tz entries";
    icu::TimeZone* t;
    facility* tf;

    for ( auto const name: names_list ) {
        auto tz_entry = get_tz_entry_by_id ( name );

        t = &get_canonical_icu_tz ( name );

        // skip if tz name was not recognized by ICU
        if ( *t == icu::TimeZone::getUnknown() ) {
            BOOST_LOG_TRIVIAL ( debug ) << "Corresponding ICU timezone was not found for TZ entry: " << name;
            continue;
        }

        // create_facility or get_facility ? that checks existed hash!
        tf = new facility ( *t, *this );

        add_facility ( name, *tf );
        add_facility ( tz_entry->std_zone_abbrev(), *tf );
        add_facility ( tz_entry->dst_zone_abbrev(), *tf, true );
    }

}

resolver_boost::btz_ptr resolver_boost::get_tz_entry_by_id ( std::string name ) {
    return db.time_zone_from_region ( name );
}

/**
 * Saves region list at the private field to avoid additional calls.
 *
 */
void resolver_boost::update_names_list() {
    names_list = db.region_list();
}

/**
 * Loads the database with CSV data
 *
 */
void resolver_boost::fill_db() {

    std::istringstream ifs ( std::string (
                                 &_binary_date_time_zonespec_csv_start,
                                 &_binary_date_time_zonespec_csv_end
                                 - &_binary_date_time_zonespec_csv_start ) );

    // skip first line (CSV header) out of the stream
    ifs.ignore ( std::numeric_limits<std::streamsize>::max(), '\n' );

    db.load_from_stream ( ifs );
}

facility& resolver_boost::best_match ( const std::string& name ) {
    auto matches = list ( name );
    if ( matches.empty() ) {
        throw -1;
    } else {
        return *matches[0];
    }
}

tzfys resolver_boost::list ( std::string ambiguous_name ) {
    tzfys founded;
    typedef std::multimap<std::string,std::pair<tzfy_ptr,bool>>::iterator iter;
    std::pair <iter, iter> ret;

    UDate cur_dt = now();
    std::string temp;

    // retrieve suspected timezones from hash
    ret = facilities_map.equal_range ( ambiguous_name );
    for ( auto pair = ret.first; pair != ret.second; ++pair ) {
        // skip DST antipodes
        if (pair->second.second != pair->second.first->dst()) {
            continue;
        }
        // skip non-matched abbreviations
        temp = pair->second.first->format_datetime ( cur_dt, "zzz" );
        if ( temp != ambiguous_name && pair->second.first->pretty() ) {
            continue;
        }
        // skip some garbage zones
        // NOTE: good abbreviations may be skipped ?
        temp = pair->second.first->format_datetime ( cur_dt, "VV" );
        if ( temp.find ( "SystemV/" ) == 0 || temp.find ( "/" )
                == std::string::npos ) {
            continue;
        }
        // add to list
        founded.push_back ( pair->second.first );
    }

    // group and filter by offsets
    std::map<int, tzfy_ptr> variants;
    for ( auto cur_facility: founded ) {
        int offset = cur_facility->calendar().get (
                         UCalendarDateFields::UCAL_ZONE_OFFSET, status )
                     + cur_facility->calendar().get (
                         UCalendarDateFields::UCAL_DST_OFFSET, status );

        try {
            tzfy_ptr stored_facility = variants.at ( offset );
            // good abbr already presented for this offset
            if ( stored_facility->pretty() ) {
                if ( cur_facility->pretty() && cur_facility->match_iana_name(ambiguous_name)) {
                    variants[offset] = cur_facility;
                }
                continue;
            }
            // if this tz has good abbr, or it's an ICU recognized,
            // replace to it in the hash
            if ( cur_facility->pretty() || cur_facility->match_iana_name(ambiguous_name)) {
                variants[offset] = cur_facility;
            }
            // not presented yet
        } catch ( const std::out_of_range& oor ) {
            variants[offset] = cur_facility;
        }
    }

    // create final list where abbr-supported tzs moved to front
    tzfys pretty_zones;
    tzfys gmt_zones;
    for ( auto variant_pair: variants ) {
        tzfy_ptr variant = variant_pair.second;
        tzfys& container = variant->pretty() ? pretty_zones : gmt_zones;
        if (variant->match_iana_name(ambiguous_name)) {
            container.insert ( container.begin(), variant );
        } else {
            container.push_back ( variant );
        }
    }
    
    pretty_zones.insert( pretty_zones.end(), gmt_zones.begin(), gmt_zones.end() );

    return pretty_zones;
}

void resolver_boost::build_locales () {
    strings raw_list = iso_countries();
    std::string lng = lang;

    std::transform (
        raw_list.begin(),
        raw_list.end(),
        std::inserter ( locales, locales.end() ),
    [&] ( const std::string& name ) {
        return std::make_pair
               ( name, locale_ptr ( new icu::Locale
                                    ( lng.c_str(), name.c_str() ) ) );
    }
    );
}

void resolver_boost::create_tz_formatters ( const std::string& fmt, UErrorCode& status ) {

    std::transform (
        locales.begin(),
        locales.end(),
        std::inserter ( tz_formatters, tz_formatters.end() ),
    [&] ( std::pair<const std::string, locale_ptr> &le ) { // TODO: locale reference
        return std::make_pair ( le.first, get_formatter ( fmt, *le.second, status ) );
    }
    );
}

strings resolver_boost::abbr_supported_countries ( icu::TimeZone& t ) const {

    UnicodeString uresult;
    strings cs_list;

    for ( auto f: tz_formatters ) {
        f.second->setTimeZone ( t );
        f.second->format ( now(), uresult );
        if ( uresult.indexOf ( "GMT" ) != 0 ) {
            cs_list.push_back ( f.first );
        }
        uresult.remove();
    }
    return cs_list;
}

std::string resolver_boost::friendly_country ( const icu::TimeZone& t ) const {

    UnicodeString uresult;
    strings cs_list;

    for ( auto f: tz_formatters ) {
        f.second->setTimeZone ( t );
        f.second->format ( now(), uresult );
        if ( uresult.indexOf ( "GMT" ) != 0 ) {
            return f.first;
        }
        uresult.remove();
    }
    return "GB";
}


facility::facility ( const icu::TimeZone& t, const resolver& r ) {
    setup ( t, *r.locales.at ( r.friendly_country ( t ) ) );
}


facility::facility ( icu::TimeZone& t, icu::Locale& l ) {
    setup ( t,l );
}

facility::facility ( icu::Calendar& cal ) {
    facility::cal = std::unique_ptr<icu::Calendar> ( &cal );
    itz = std::unique_ptr<const icu::TimeZone> ( &facility::cal->getTimeZone() );
    loc = std::make_unique<icu::Locale> (
              facility::cal->getLocale ( ULOC_ACTUAL_LOCALE, status ) );
}

void facility::setup ( const icu::TimeZone& t, icu::Locale& l ) {
    itz = std::unique_ptr<const icu::TimeZone> ( &t );
    loc = std::make_unique<icu::Locale> ( l );
    cal = std::unique_ptr<icu::Calendar> ( Calendar::createInstance ( t, l, status ) );
}

const icu::TimeZone& facility::tz() const {
    return *itz;
}

const bool facility::match_iana_name ( const std::string& name ) const {
    return resolver_boost::get_canonical_icu_tz ( name ) == tz();
}

const icu::Calendar& facility::calendar() const {
    return *cal;
}

const UDate facility::now() const {
    return calendar().getNow();
}

const bool facility::dst() const {
    UErrorCode status = U_ZERO_ERROR;
    return calendar().get(UCalendarDateFields::UCAL_DST_OFFSET, status) > 0;
}

const bool facility::pretty() const {
    return ( format_datetime ( now(), "zzz" ).find ( "GMT" ) != 0 );
}

std::string facility::format_datetime ( const UDate& dt, const std::string& format ) const {
    UErrorCode status = U_ZERO_ERROR;
    auto fm = get_formatter ( format, *loc, status );

    std::string result;
    UnicodeString uresult;

    fm->setTimeZone ( *itz );
    fm->format ( dt, uresult );
    return uresult.toUTF8String ( result );
}

};




