#include <algorithm>
#include <cctype>
#include <iterator>
#include <boost/format.hpp>
#include "boost/algorithm/string/case_conv.hpp"
#include "boost/algorithm/string/trim.hpp"
#include "boost/algorithm/string/join.hpp"
#include "unicode/ucal.h"

#include "dialog.hpp"


namespace tzutils {
    
std::string dt_line(const std::string& format, const UDate& dt,  facility& fcty, const bool& debug = false) {
    UErrorCode status = U_ZERO_ERROR;
    
    if (debug) {
        return boost::str(boost::format("%1% [%2% : %3% : %4% ]")
        % fcty.format_datetime ( dt, format )
                % (fcty.calendar().get(UCalendarDateFields::UCAL_ZONE_OFFSET, status) / (1000*60*60.0))
                % (fcty.calendar().get(UCalendarDateFields::UCAL_DST_OFFSET, status) / (1000*60*60.0))
                % (fcty.calendar().inDaylightTime(status) ? 1 : 0)
            ).c_str();
    } else {
        return boost::str(boost::format("%1%")
        % fcty.format_datetime ( dt, format )
            ).c_str();
    }
}
    
    
std::string dialog::ambiguous_name_to_now_datetime ( std::string name, resolver& rslvr,
        const std::string& format, const bool verbose ) {
    // normalize user input
    boost::algorithm::trim ( name );
    boost::algorithm::to_upper ( name);
    
    try {
        auto& fcty = rslvr.best_match ( name );  // try to find tz
        return dt_line(format, tzutils::now(), fcty, verbose);
    } catch (int e) {
        return MSG_NO_TZ_FOUND;
    }
}
    
    
std::string dialog::ambiguous_name_to_now_datetimes ( std::string ambiguous_name, resolver& rslvr,
        const std::string& format, const bool verbose ) {

    // normalize user input
    boost::algorithm::trim ( ambiguous_name );
    boost::algorithm::to_upper ( ambiguous_name);

    // try to find tzs
    auto facilities = rslvr.list ( ambiguous_name );

    if ( facilities.empty() ) {
        return MSG_NO_TZ_FOUND;
    }

    // process founded tzs
    
    strings dt_strings;
    UDate cur_dt = tzutils::now();

    // map founded facilities to formatted datetime strings
    std::transform (
        facilities.begin(),
        facilities.end(),
        std::back_inserter ( dt_strings ),
    [&] ( auto fct ) {
        return dt_line(format, cur_dt, *fct, verbose);
    }
    );

    return boost::algorithm::join(dt_strings, "\n");
}

}


/*
 *
 * t->getDisplayName(false, icu::TimeZone::EDisplayType::SHORT , tstr))

}*/
