

#ifndef TZNEGOTIATOR_HEADERS

#define TZNEGOTIATOR_HEADERS

#include <string>

#include "unicode/calendar.h"
#include "unicode/smpdtfmt.h"

#include "resolver.hpp"

#define DEFAULT_DT_FORMAT "E MMM d HH:mm:ss zzz [zzzz] y [VV: VVVV]"


#define MSG_NO_TZ_FOUND "Based on your input, there are no timezones \
found at all or they aren't observed at this moment."


namespace tzutils {

/**
 * This static class provides methods related to "platform"-independed
 * negotiations with the end user.
 *
 * Intended to be used by real utilities (CLI, TCP/IP etc.)
 *
 */
class dialog {
public:
    /**
     * For a given ambiguous timezone name
     * returns a list of current datetimes adjusted for timezones
     * which the user might had in mind.
     *
     * If no timezones found for a given name, returns a message about this.
     *
     * @param ambiguous_name a timezone name represented as a "raw" user data, i. e.
     * with whitespaces and possibly inconsistent character case
     * @param format a datetime format string as described for ICU::SimpleDateFormat
     * @param rslvr a resolver object that would be used to find timezones
     * @return a human-readable list of datetimes
     *
     */
    static std::string ambiguous_name_to_now_datetimes (
        std::string ambiguous_name, resolver& rslvr,
        const std::string& format = DEFAULT_DT_FORMAT, const bool verbose = false );

    /**
     * For a given timezone name
     * returns current datetime adjusted for best-matched timezone
     *
     * If no timezone found for a given name, returns a message about this.
     *
     * @param name a timezone name represented as a "raw" user data, i. e.
     * with whitespaces
     * @param format a datetime format string as described for ICU::SimpleDateFormat
     * @param rslvr a resolver object that would be used to find timezones
     * @return a human-readable datetime or error message
     *
     */
    static std::string ambiguous_name_to_now_datetime (
        std::string name, resolver& rslvr,
        const std::string& format = DEFAULT_DT_FORMAT, const bool verbose = false);
private:
    /**
     * Normalize user input in-place
     *
     */
    static void  normalize_user_input ( std::string& input );
};

}

#endif
