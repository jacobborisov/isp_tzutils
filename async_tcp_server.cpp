#include <ctime>
#include <iostream>
#include <string>
#include <chrono>
#include <functional>
#include "boost/program_options.hpp"
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio.hpp>
#include <boost/format.hpp>
#include <boost/log/trivial.hpp>
#include "boost/log/core.hpp"
#include "boost/log/trivial.hpp"
#include "boost/log/expressions.hpp"
#include "boost/date_time/local_time/local_time.hpp"
#include "unicode/timezone.h"
#include "unicode/locid.h"
#include "unicode/utypes.h"

#include "dialog.hpp"

#define PORT 22222
//#define DT_OUT_FORMAT "E MMM d HH:mm:ss zzz y [VV: VVVV]"
#define DT_OUT_FORMAT "E MMM d HH:mm:ss zzz y"
#define SOME_ERR_MSG "Something goes not as expected"
#define LOG_LEVEL debug

using boost::asio::ip::tcp;

typedef std::function<std::string ( std::string, tzutils::resolver&, std::string&, bool ) > process_fn;
typedef boost::shared_ptr<tzutils::resolver> resolver;

class tcp_connection : public boost::enable_shared_from_this<tcp_connection> {
public:

    typedef boost::shared_ptr<tcp_connection> pointer;

    static pointer create ( boost::asio::io_service &io_service ) {
        return pointer ( new tcp_connection ( io_service ) );
    }

    tcp::socket& socket() {
        return socket_;
    }

    void set_tz_resolver ( resolver& r ) {
        tz_resolver = r;
    }

    void set_handler ( process_fn h ) {
        resp_handler = h;
    }

    void start() {
        BOOST_LOG_TRIVIAL ( debug ) << "TCP connection established.";

        boost::asio::async_read_until ( socket_, user_input_buf, '\n',
                                        boost::bind ( &tcp_connection::handle_read_input,
                                                shared_from_this(),
                                                boost::asio::placeholders::error,
                                                boost::asio::placeholders::bytes_transferred ) );
    }
private:
    process_fn resp_handler;
    resolver tz_resolver;
    boost::asio::streambuf user_input_buf;
    tcp::socket socket_;
    std::string message_;

    tcp_connection ( boost::asio::io_service &io_service ) : socket_ ( io_service ) {
        BOOST_LOG_TRIVIAL ( debug ) << "A new TCP connection allocated.";
    }

    void handle_read_input ( const boost::system::error_code& error,
                             std::size_t length ) {

        std::string response;

        BOOST_LOG_TRIVIAL ( debug ) << "handle_read_otuput started";


        if ( !error ) {
            std::istream is ( &user_input_buf );
            std::getline ( is, message_ );
            std::string fmt = DT_OUT_FORMAT;
            response = resp_handler ( message_, *tz_resolver, fmt, false ) + "\n";
        } else {
            response = std::string ( SOME_ERR_MSG ) + "\n";
        }

        BOOST_LOG_TRIVIAL ( debug ) << "message received " + message_;

        //message_ = make_daytime_string();


        boost::asio::async_write ( socket_,
                                   boost::asio::buffer ( response ),
                                   boost::bind ( &tcp_connection::handle_write, shared_from_this(),
                                           boost::asio::placeholders::error,
                                           boost::asio::placeholders::bytes_transferred ) );

        BOOST_LOG_TRIVIAL ( debug ) << "handle_read_output finished";
    }

    void handle_write ( const boost::system::error_code & /*error*/,
                        size_t /*bytes_transferred*/ ) {
        BOOST_LOG_TRIVIAL ( debug ) << "write handler finished";
    }
};

class tz_server {
public:

    tz_server ( boost::asio::io_service &io_service, process_fn fn, int port ) :
        acceptor_ ( io_service, tcp::endpoint ( tcp::v4(), port ) ) {
        process = fn;
        tz_resolver = resolver ( tzutils::resolver::create_resolver() );
        start_accept();
    }

private:
    process_fn process;
    tcp::acceptor acceptor_;
    resolver tz_resolver;

    void start_accept() {
        tcp_connection::pointer new_connection =
            tcp_connection::create ( acceptor_.get_io_service() );

        acceptor_.async_accept ( new_connection->socket(),
                                 boost::bind ( &tz_server::handle_accept, this, new_connection,
                                               boost::asio::placeholders::error ) );
    }

    void handle_accept ( tcp_connection::pointer new_connection,
                         const boost::system::error_code &error ) {
        if ( !error ) {

            new_connection->set_tz_resolver ( tz_resolver );
            new_connection->set_handler ( process );
            new_connection->start();
        }

        start_accept();
    }
};

/**
 * Creates and runs TCP server daemon
 */
int main ( int argc, char** argv ) {

    namespace po = boost::program_options;
    po::options_description desc ( "Options" );
    desc.add_options()
    ( "help", "Print help messages" )
    ( "debug", "Enable debug outputs" )
    ( "strict", "Returns single datetime, only if input exactly matches against one of the ICU recognized timezone names" )
    ( "port", po::value<int>()->default_value ( PORT )->notifier (
    [] ( std::size_t value ) {
        if ( ! ( value > 0 && value < 65535 ) ) {
            throw po::validation_error (
                po::validation_error::invalid_option_value,
                "option_name",
                std::to_string ( value ) );
        }
    } ), boost::str ( boost::format ( "Specify TCP port the server listen on, default %1%" ) % PORT ).c_str() );


    po::variables_map vm;
    try {
        po::store ( po::parse_command_line ( argc, argv, desc ), vm );
    }  catch ( po::error& e ) {
        std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
        std::cerr << desc << std::endl;
        return 1;
    }
    po::notify ( vm );

    boost::log::core::get()->set_filter
    (
        boost::log::trivial::severity >= ( vm.count ( "debug" ) ?
                                           boost::log::trivial::debug
                                           : boost::log::trivial::info )
    );

    if ( vm.count ( "help" ) ) {
        std::cout << desc << "\n";
        return 1;
    }


    process_fn process = vm.count ( "strict" ) ?
                         tzutils::dialog::ambiguous_name_to_now_datetime
                         : tzutils::dialog::ambiguous_name_to_now_datetimes;

    try {
        boost::asio::io_service io_service;
        tz_server server ( io_service, process, vm["port"].as<int>() );
        io_service.run();
    } catch ( std::exception& e ) {
        std::cerr << "ERROR: " << std::endl;
        std::cerr << e.what() << std::endl;
    }

    return 0;

}
