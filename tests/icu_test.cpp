#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch/catch.hpp"
#include <boost/algorithm/string.hpp>
#include "unicode/timezone.h"

#include "../dialog.hpp"

TEST_CASE("ICU lib test", "[icu_test]") {
    UErrorCode status = U_ZERO_ERROR;
    UnicodeString id = "Europe/Belfast", cid;
    std::string result;
    //icu::TimeZone* t = icu::TimeZone::createTimeZone ( "Europe/Belfast" );
    icu::TimeZone::getCanonicalID(id, cid, status);
    cid.toUTF8String(result);
    REQUIRE(result == "Europe/London");
}

TEST_CASE("Strict mode test", "[strict_test]") {
    

    auto r = tzutils::resolver::create_resolver();

    auto result = tzutils::dialog::ambiguous_name_to_now_datetime ("IST", *r, "zzz");
    
    REQUIRE(result.find("IST") != std::string::npos);
}

TEST_CASE("Standard mode test", "[main_test]") {
    
    auto r = tzutils::resolver::create_resolver();

    auto result = tzutils::dialog::ambiguous_name_to_now_datetimes ("BST", *r, "zzz");
    
    std::vector<std::string> results;
 
    boost::split(results, result, [](char c){return c == '\n';});
    
    REQUIRE(results.size() == 3);
    
}
