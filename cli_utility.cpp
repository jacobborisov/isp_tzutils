#include <iostream>
#include <functional>
#include "boost/program_options.hpp"
#include "boost/log/core.hpp"
#include "boost/log/trivial.hpp"
#include "boost/log/expressions.hpp"
#include "unicode/timezone.h"

#include "dialog.hpp"

//#define LOG_LEVEL info
//#define DT_OUT_FORMAT "E MMM d HH:mm:ss zzz y [VV: VVVV]"
#define DT_OUT_FORMAT "E MMM d HH:mm:ss zzz y"


int main ( int argc, char** argv ) {

    namespace po = boost::program_options;
    po::options_description desc ( "Options" );
    desc.add_options()
    ( "help", "Print help messages" )
    ( "debug", "Enable debug outputs" )
    ( "strict", "Returns single datetime, only if input exactly matches against one of the ICU recognized timezone names" );

    po::variables_map vm;
    try {
        po::store ( po::parse_command_line ( argc, argv, desc ), vm );
    }  catch ( po::error& e ) {
        std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
        std::cerr << desc << std::endl;
        return 1;
    }
    po::notify ( vm );

    boost::log::core::get()->set_filter
    (
        boost::log::trivial::severity >= ( vm.count ( "debug" ) ?
                                           boost::log::trivial::debug : boost::log::trivial::info )
    );

    if ( vm.count ( "help" ) ) {
        std::cout << desc << "\n";
        return 1;
    }

    std::string user_input;

    std::cin >> user_input;


    auto r = tzutils::resolver::create_resolver();

    std::function<std::string ( std::string, tzutils::resolver&,
                                std::string&, bool ) > process =
                                    vm.count ( "strict" ) ?
                                    tzutils::dialog::ambiguous_name_to_now_datetime
                                    : tzutils::dialog::ambiguous_name_to_now_datetimes;

    std::string fmt = DT_OUT_FORMAT;

    std::cout << process ( user_input, *r, fmt, vm.count ( "debug" ) ) << std::endl;

    return 0;

}
